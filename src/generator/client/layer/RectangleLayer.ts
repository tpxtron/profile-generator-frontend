import { ColorLayer } from "./ColorLayer";
import { LayerDrawer } from "./Layer";

export type RectangleLayer = {
  type: "RECTANGLE";
  offset: number;
  size: number;
} & ColorLayer;

export const drawRectangle: LayerDrawer<RectangleLayer> = (
  ctx,
  { offset, size, color }
) => {
  ctx.save();
  ctx.fillStyle = color;
  ctx.fillRect(offset, offset, size, size);
  ctx.restore();
};
