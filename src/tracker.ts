export type PiwikTracker = {
  trackEvent: (
    category: string,
    action: string,
    name?: string,
    value?: number
  ) => void;
};

export async function createTracker(
  container: HTMLElement,
  trackerHost: string,
  siteId: number
): Promise<PiwikTracker> {
  // Load Piwik if not already loaded
  if (!(window as any).Piwik) {
    let g = document.createElement("script");
    g.type = "text/javascript";
    g.async = true;
    g.defer = true;
    g.src = trackerHost + "piwik.js";
    container.appendChild(g);

    // Wait for Piwik to be loaded
    await new Promise((resolve, reject) => {
      (window as any).piwikAsyncInit = function() {
        resolve();
      };
    });
  }

  // Initialize the tracker
  const piwikTracker = (window as any).Piwik.getTracker();
  piwikTracker.setSiteId(siteId);
  piwikTracker.setTrackerUrl(trackerHost + "piwik.php");
  piwikTracker.trackPageView();
  return piwikTracker;
}
